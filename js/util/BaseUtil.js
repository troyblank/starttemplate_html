var BaseUtil = {

    IS_TOUCH_DEVICE: !!('ontouchstart' in window),

    //---------------------------------------------------------------------------------------------
    // MEDIA QUERY LOOKUP
    //---------------------------------------------------------------------------------------------

    getMediaQuerySize: function() {
        try {
            var size = window.getComputedStyle(document.body, ':before').getPropertyValue('content').replace(/"/g, '');
            return size
        } catch (e) {
            //DOES NOT WORK IN IE 8
            return 'desktop';
        }
    },

    //---------------------------------------------------------------------------------------------
    // VALIDATION
    //---------------------------------------------------------------------------------------------
    contentTest: function(value) {
        if (value === null || value === undefined) {
            return false;
        }
        return (/\S/.test(value));
    }
}