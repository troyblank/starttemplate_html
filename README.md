# startTemplate_HTML
This is a start template to be used by a front end developer as a good base to begin a project. All automation is handled with grunt on node.js.

##Dependencies
* node.js
* grunt-cli

##Included start points:
* general folder and html structure
* sass watchin and deployment
* uglify watching and deployment
* grunticon svg to sass process

## Installation

First if you don't have Grunt installed run:

    npm install -g grunt-cli
    
Sass is needed to generate css run

    gem install sass -v 3.3.3
    
To install all Node dependencies run:

    npm install

## Commands

To generate all static assets needed run:

    grunt deploy

To generate static assets on the fly when coding run:

    grunt

To test all js run:

    grunt test